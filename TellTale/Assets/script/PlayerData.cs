﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MyScriptable/Create PlayerData")]
public class PlayerData : ScriptableObject
{

	public string playerName;
	public int maxHp;
	public int level;
	public int atk;
	public int def;
	public int exp;
	public int gold;


}